import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PdfComponent } from './pdf/pdf.component';
import { CsvComponent } from './csv/csv.component';
import { ExcelComponent } from './excel/excel.component';
import { RouterModule, Routes } from '@angular/router';
import { DatatablesService } from '../tables/datatables/datatables.service';
import { CoreCommonModule } from '@core/common.module';
import { CardSnippetModule } from '@core/components/card-snippet/card-snippet.module';
import { CsvModule } from '@ctrl/ngx-csv';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ContentHeaderModule } from 'app/layout/components/content-header/content-header.module';

const routes: Routes = [
  {
    path: 'pdf',
    component: PdfComponent,
    resolve: {
      datatables: DatatablesService
    },
    data: { animation: 'datatables' }
  },
  {
    path: 'csv',
    component: CsvComponent,
    resolve: {
      datatables: DatatablesService
    },
    data: { animation: 'datatables' }
  },
  {
    path: 'excel',
    component: ExcelComponent,
    resolve: {
      datatables: DatatablesService
    },
    data: { animation: 'datatables' }
  }
]

@NgModule({
  declarations: [
    PdfComponent,
    CsvComponent,
    ExcelComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgbModule,
    TranslateModule,
    CoreCommonModule,
    ContentHeaderModule,
    CardSnippetModule,
    NgxDatatableModule,
    CsvModule
  ],
  providers: [DatatablesService]
})
export class ExportModule { }
