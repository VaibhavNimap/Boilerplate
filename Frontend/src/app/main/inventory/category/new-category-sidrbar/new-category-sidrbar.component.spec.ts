import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCategorySidrbarComponent } from './new-category-sidrbar.component';

describe('NewCategorySidrbarComponent', () => {
  let component: NewCategorySidrbarComponent;
  let fixture: ComponentFixture<NewCategorySidrbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewCategorySidrbarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewCategorySidrbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
