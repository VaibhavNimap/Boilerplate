import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewProductSidebarComponent } from './new-product-sidebar.component';

describe('NewProductSidebarComponent', () => {
  let component: NewProductSidebarComponent;
  let fixture: ComponentFixture<NewProductSidebarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewProductSidebarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewProductSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
