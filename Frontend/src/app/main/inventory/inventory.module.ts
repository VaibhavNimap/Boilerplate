import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product/product.component';
import { CategoryComponent } from './category/category.component';
import { RouterModule, Routes } from '@angular/router';
import { NewCategorySidrbarComponent } from './category/new-category-sidrbar/new-category-sidrbar.component';
import { NewProductSidebarComponent } from './product/new-product-sidebar/new-product-sidebar.component';
import { ProductListService } from './product/product-list.service';
import { CategoryListService } from './category/category-list.service';
import { FormsModule } from '@angular/forms';
import { CoreCommonModule } from '@core/common.module';
import { CoreSidebarModule } from '@core/components';
import { CoreDirectivesModule } from '@core/directives/directives';
import { CorePipesModule } from '@core/pipes/pipes.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { Ng2FlatpickrModule } from 'ng2-flatpickr';
import { InvoiceModule } from '../apps/invoice/invoice.module';

const routes: Routes = [
  {
    path: 'product',
    component: ProductComponent,
    resolve: {
       uls: ProductListService
    },
    data: { animation: 'productComponent' }
  },
  {
    path: 'category',
    component: CategoryComponent,
    resolve: {
      uls: CategoryListService
    },
    data: { animation: 'categoryComponent' }
  },
];

@NgModule({
  declarations: [
    ProductComponent,
    CategoryComponent,
    NewCategorySidrbarComponent,
    NewProductSidebarComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CoreCommonModule,
    FormsModule,
    NgbModule,
    NgSelectModule,
    Ng2FlatpickrModule,
    NgxDatatableModule,
    CorePipesModule,
    CoreDirectivesModule,
    InvoiceModule,
    CoreSidebarModule
  ],
  providers:[ProductListService,CategoryListService]
})
export class InventoryModule { }
