import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { ApiComponent } from './api/api.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CoreCommonModule } from '@core/common.module';
import { CoreSidebarModule } from '@core/components';
import { CoreDirectivesModule } from '@core/directives/directives';
import { CorePipesModule } from '@core/pipes/pipes.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { Ng2FlatpickrModule } from 'ng2-flatpickr';
import { InvoiceModule } from '../apps/invoice/invoice.module';
import { NewLogsComponent } from './new-logs/new-logs.component';
import { UserListService } from '../apps/user/user-list/user-list.service';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    resolve: {
      uls: UserListService
    },
    data: { animation: 'LoginComponent' }
  },
  {
    path: 'api',
    component: ApiComponent,
    resolve: {
      uls: UserListService
    },
    data: { animation: 'ApiComponent' }
  },
];

@NgModule({
  declarations: [
    LoginComponent,
    ApiComponent,
    NewLogsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CoreCommonModule,
    FormsModule,
    NgbModule,
    NgSelectModule,
    Ng2FlatpickrModule,
    NgxDatatableModule,
    CorePipesModule,
    CoreDirectivesModule,
    InvoiceModule,
    CoreSidebarModule
  ],
  providers: [UserListService]
})
export class LogsModule { }
